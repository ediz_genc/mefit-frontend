# MeFit App User Documentation

Welcome to the MeFit Fitness App User Documentation. This guide will help you navigate through the application and understand its features.

## Table of Contents

1. [Introduction](#introduction)
2. [Logging In](#logging-in)
3. [Registering a User](#registering-a-user)
4. [Dashboard](#dashboard)
5. [Fitness Content](#fitness-content)
6. [Profile](#profile)
7. [Navigation](#navigation)
8. [Security Considerations](#security-considerations)
9. [Conclusion](#conclusion)

---

## 1. Introduction

The MeFit Fitness App empowers users with personalized guidance, accountability, motivation, and community support to achieve their fitness aspirations.

---

## 2. Logging In

To access the MeFit Fitness App:

- Visit the login page.
- Click the "Login" button.
- Enter your credentials (username and password).

<div align="center">
  <center><img src="src/assets/images/image.png" width="1000"/></center>
</div>
<br>
<div align="center">
  <center><img src="src/assets/images/image-1.png" width="1000"/></center>
</div>
---

## 3. Registering a User

To create a new user account:

- Visit the registration page.
- Fill out the required information.
- Click the "Register" button.

<div align="center">
  <center><img src="src/assets/images/image-2.png" width="1000"/></center>
</div>
---

## 4. Create a profile

To create a new profile: 

- Fill out the required information.
- Click the "Accept button".

<div align="center">
  <center><img src="src/assets/images/image-3.png" width="1000"/></center>
</div>

---

## 5. Dashboard

The dashboard is your central hub for tracking your fitness goals and progress:

- Users can set goals for the week and view their progress.
- It displays an overview of your progress toward achieving your weekly goals.
- A calendar shows the current day and remaining days in the week.

<div align="center">
  <center><img src="src/assets/images/image-4.png" width="1000"/></center>
</div>
<br>
<div align="center">
  <center><img src="src/assets/images/image-5.png" width="1000"/></center>
</div>
---

## 6. Profile

Access and edit your user profile:

- View and edit your name, profile picture, bio, and fitness preferences.
- Update other relevant information, such as height and weight.
- View your goal history.

<div align="center">
  <center><img src="src/assets/images/image-6.png" width="1000"/></center>
</div>

---

## 7. Navigation

The navigation bar contains:

- An indicator of the currently logged-in user.
- A logout button.
- Links to access different components of the application.

<div align="center">
  <center><img src="src/assets/images/image-7.png" width="1000"/></center>
</div>

---

## 8. Security Considerations

Please be aware of security features and considerations in the app for your safety and privacy.

---

## 9. Conclusion

This concludes our user documentation for the MeFit Fitness App. We hope this guide helps you make the most of our application.

---
